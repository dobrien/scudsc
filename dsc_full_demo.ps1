﻿$ConfigData= 
@{ 
    AllNodes =
        @(
            @{ 
                NodeName= 'localhost';
                Role = 'SiteServer'
            }
        );
}

configuration test {
    param(
        )


    Node $AllNodes.NodeName {
            WindowsFeature WebServer
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Server'
                    #IncludeAllSubFeature = "true"  
                }
            WindowsFeature IISConsole
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Mgmt-Console'
                    DependsOn = '[WindowsFeature]WebServer'  
                }
            WindowsFeature IISBasicAuth
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Basic-Auth'
                    DependsOn = '[WindowsFeature]WebServer'
                }
            WindowsFeature IISWindowsAuth
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Windows-Auth'
                    DependsOn = '[WindowsFeature]WebServer'
                }
            WindowsFeature IISURLAuth
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Url-Auth'
                    DependsOn = '[WindowsFeature]WebServer'
                }
            WindowsFeature ASPNet45
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'Web-Asp-Net45'
                    DependsOn = '[WindowsFeature]WebServer'
                }
            WindowsFeature RDC
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'RDC'
                }
            WindowsFeature BITS
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'BITS'
                }
            WindowsFeature DotNet45Features
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'NET-Framework-45-Features'
                }
            WindowsFeature DotNet45Core
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'NET-Framework-45-Core'
                    DependsOn = '[WindowsFeature]DotNet45Features'
                }
            WindowsFeature NetWCFHTTPActivation
                {
                    Ensure = 'Present' # To uninstall the role, set Ensure to "Absent"
                    Name = 'NET-WCF-HTTP-Activation45'
                    DependsOn = '[WindowsFeature]DotNet45Features'
                }
        }
    }


test -ConfigurationData $ConfigData -OutputPath C:\tmp\test -Verbose

#Start-DscConfiguration -Wait -ComputerName localhost -Path c:\tmp\test -Verbose