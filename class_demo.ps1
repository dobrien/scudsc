﻿class WindowsService {
  [ValidatePattern('[0-9a-zA-Z]{1,30}')]
  [string] $Name

  [ValidateSet('Auto', 'Manual')]
  [string] $StartupType


  WindowsService([string] $Name) {

    if ($Name -notmatch '[0-9a-zA-Z]{1,30}') {
      throw 'Invalid Service Name'
    }

    $Service = Get-CimInstance -ClassName Win32_Service -Filter "Name = '$Name'"

    $this.StartupType = $Service.StartType
  }
}

[WindowsService]::new('winmgmt')
