﻿configuration CM12 {
    
    Import-DscResource -ModuleName ConfigMgrClass

    ConfigMgrClass Primary {
        SiteCode = 'CM1'
        SiteName =         'Test'
        DPServer = 'CM002.psftw.local'
        MPServer = 'CM002.psftw.local'
        SourcePath = '\\adds\Deployment\Source\SystemCenter2012R2\ConfigurationManager'
        InstallationDirectory = 'D:\CM12'
        SMSProviderServer = 'cm002.psftw.local'
        SQLServer = 'cm002.psftw.local'
        PrereqPath = "$env:windir\temp"
    }

}

CM12 -OutputPath C:\temp\CM12class