﻿enum Ensure {
    Absent
    Present
}

[DscResource()]
class ConfigMgrClass
{
    [DscProperty(Key)]
    [string] $SiteCode 
    [DscProperty(Key)]
    [string] $SiteName
    [DscProperty(Mandatory)]
    [string] $DPServer
    [DscProperty(Mandatory)]
    [string] $MPServer
    [DscProperty(Mandatory)]
    [string] $SourcePath
    [DscProperty(Mandatory)]
    [string] $InstallationDirectory
    [DscProperty(Mandatory)]
    [string] $SMSProviderServer
    [DscProperty(Mandatory)]
    [string] $SQLServer
    [string] $SQLServerInstance
    [Uint32] $SQLPort
    [DscProperty(Mandatory)]
    [string] $PrereqPath
    [Ensure] $Ensure

        [void] Set()
        {
        if ([string]::IsNullOrEmpty($this.SiteName)) {
            $this.SiteName = "Primary Site $($this.SiteCode)"
        }
        if ([string]::IsNullOrEmpty($this.PrereqPath)) {
            $this.PrereqPath = $(Join-Path $env:SystemDrive CM12Prereqs)
        }
        if ([string]::IsNullOrEmpty($this.InstallationDirectory)) {
            $this.InstallationDirectory = $(Join-Path $env:SystemDrive CM12)
        }
        if ([string]::IsNullOrEmpty($this.MPServer)) {
            $this.MPServer = ([System.Net.Dns]::GetHostByName(($env:computerName))).HostName
        }
        if ([string]::IsNullOrEmpty($this.DPServer)) {
            $this.DPServer = ([System.Net.Dns]::GetHostByName(($env:computerName))).HostName
        }

        Create-InstallINI -SiteCode $this.SiteCode -SiteName $this.SiteName -DPServer $this.DPServer -MPServer $this.MPServer -InstallationDirectory $this.InstallationDirectory -SMSProviderServer $this.SMSProviderServer -SQLServer $this.SQLServer -SQLPort $this.SQLPort -PrereqPath $this.PrereqPath -SQLServerInstance $this.SQLServerInstance;


        Write-Verbose "Installing ConfigMgr Primary Site $this.SiteCode"

        $Process = @{
            FilePath = '{0}\SMSSETUP\BIN\X64\setup.exe' -f $this.SourcePath;
            ArgumentList = '/Script "{0}\temp\CM12Unattend.ini" /NoUserInput' -f $env:windir;
            Wait = $true;
            PassThru = $true;
            RedirectStandardOutput = '{0}\temp\CM12-StdOut.txt' -f $env:windir;
        }
        $Proc = Start-Process @Process;
        $Proc.WaitForExit()

        # Tell the DSC Engine to restart the machine
        $global:DSCMachineStatus = 1
        }
        
        [bool] Test()
        {
            $Get = $this.Get
            
            if ($this.SiteCode -eq $Get.SiteCode) {
                return $true
            }
            else {
                return $false
            }
        
        }

        [ConfigMgrClass] Get() {

            $Configuration = [hashtable]::new()
            $Configuration.Add('SiteCode', $this.SiteCode)

            try {
                $CMSite = Get-CimInstance -ClassName SMS_Site -Namespace root\SMS\Site_$($this.SiteCode) -ErrorAction Stop

                if ($CMSite) {
                    $Configuration.Add('Ensure','Present')
                }
                else {
                    $Configuration.Add('Ensure','Absent')
                }
            }
            catch {
                $exception = $_

                 Write-Verbose 'Error occurred'

                 while ($exception.InnerException -ne $null)

                 {
                     $exception = $exception.InnerException
                     Write-Verbose $exception.message
                 }
                }    
            return $Configuration
        }

}

#region Helper Functions

Function Create-InstallINI {

    param
    (	
        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $SiteCode,
	    
        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $SiteName,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $DPServer,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $MPServer,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $InstallationDirectory,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $SMSProviderServer,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $SQLServer,
        
        [parameter(Mandatory=$false)]
        [string] $SQLServerInstance = '',

        [ValidateRange(1000,9999)]
        [Uint32] $SQLPort = 1433,

        [parameter(Mandatory)]
        [ValidateNotNullOrEmpty()]
        [string] $PrereqPath

    )

    if ([string]::IsNullOrEmpty($SQLServerInstance)) {
        $DBName = "CM_$SiteCode"
    }
    else {
        $DBName = "$SQLServerInstance\CM_$SiteCode"
    }

    $Ini = @"
[Identification]
Action=InstallPrimarySite

[Options]
ProductID=EVAL
SiteCode=$SiteCode
SiteName=$SiteName
SMSInstallDir=$InstallationDirectory
SDKServer=$SMSProviderServer
RoleCommunicationProtocol=HTTPorHTTPS
ClientsUsePKICertificate=0
PrerequisiteComp=0
PrerequisitePath=$PrereqPath
MobileDeviceLanguage=0
ManagementPoint=$MPServer
ManagementPointProtocol=HTTP
DistributionPoint=$DPServer
DistributionPointProtocol=HTTP
DistributionPointInstallIIS=0
AdminConsole=1

[SQLConfigOptions]
SQLServerName=$SQLServer
DatabaseName=$DBName
SQLSSBPort=4022

[HierarchyExpansionOption]
"@

    $AnswerFile = '{0}\temp\CM12Unattend.ini' -f $env:windir;
    Set-Content -Path $AnswerFile -Value $Ini;
    Write-Verbose -Message ('Finished writing INI file to: {0}' -f $AnswerFile);

}
#endregion Helper Functions