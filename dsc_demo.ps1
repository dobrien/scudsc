﻿[DscLocalConfigurationManager()]

Configuration LCMSettings {

    Node localhost

    {

        Settings

        {

            RefreshMode = “Disabled”

        }

    }

}

LCMSettings

Set-DscLocalConfigurationManager -Path .\LCMSettings -Verbose 

Invoke-DscResource -Name File -Method Set -Property @{DestinationPath="C:\temp";Ensure="Present";Type="Directory"} -ModuleName PSDesiredStateConfiguration -Verbose


$destinationPath=“$env:SystemDrive\DirectAccess.txt”;
$fileContents=‘This file is create by Invoke-DscResource’
$result = Invoke-DscResource -Name File -Method Set -Property @{ DestinationPath=$destinationPath;Contents=$fileContents } -ModuleName PSDesiredStateConfiguration -Verbose
$result.ItemValue | Format-List * 

